import Vue from 'vue'
import VueRouter from 'vue-router'
import Navbar from '../views/Navbar'

const DashboardRoom = () => import("@/views/DashboardRoom.vue")
const ListRoom = () => import("@/views/ListRoom.vue")
const login = () => import("@/views/Login.vue")

Vue.use(VueRouter)

const routes = [
  { path: '/login',name: "login", component: login },
  {
    path: "/",
    name: "Navbar",
    component: Navbar,
    children: [
      {
        path: "/ListRoom",
        name: "ListRoom",
        component: ListRoom
      },
      {
        path: "/DashboardRoom",
        name: "DashboardRoom",
        component: DashboardRoom
      },
  { path: '/*', redirect: '/login' },
    ]
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;

 

// import Vue from 'vue'
// import VueRouter from 'vue-router'
// import HomePage from '../views/HomePage.vue'
// import Login from '../views/Login.vue'
// import Navbar from '../views/Navbar.vue'

// // const PRDetail = () => import("@/components/HomePage/PRDetail.vue");



// Vue.use(VueRouter)

// const routes = [
//   { path: '/Login', component: Login },
//   {
//     path: "/",
//     name: "Navbar",
//     component: Navbar,
//     children: [

//       {
//         path: "/HomePage",
//         name: "HomePage",
//         component: HomePage
//       },


//       {
//         path: "/",
//         name: "noPath",
//         redirect: '/Login',
//         component: Login
//       },
//       {
//         path: "/*",
//         redirect: '/Login',
//         component: Login
//       },
//     ]
    
//   },
//   { path: '/*', redirect: '/Login' },
// ]

// const router = new VueRouter({
//   mode: 'history',
//   base: process.env.BASE_URL,
//   routes
// })

// export default router
