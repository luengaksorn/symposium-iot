import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'material-icons/iconfont/material-icons.css';
import VueSweetalert2 from 'vue-sweetalert2';
import axios from 'axios'
import VueAxios from 'vue-axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import { DatePicker } from 'ant-design-vue';
import VueApexCharts from 'vue-apexcharts'
import VueGauge from 'vue-gauge';
import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import Column2D from 'fusioncharts/fusioncharts.charts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

Vue.use(VueFusionCharts, FusionCharts, Column2D, FusionTheme);
  Vue.component('vue-gauge', VueGauge);
Vue.use(DatePicker);
Vue.use(Antd);
Vue.component('apexchart', VueApexCharts)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.use(VueSweetalert2);
new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')


// require styles
require("./assets/css/main.css");
require("./assets/fonts/stylesheet.css");